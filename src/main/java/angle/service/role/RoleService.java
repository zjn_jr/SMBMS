package angle.service.role;

import angle.pojo.Role;

import java.util.List;

public interface RoleService {
    /*获取角色列表*/
    List<Role> getRoleList();

    /*通过id获取角色*/
    Role getRoleById(int id);
}
