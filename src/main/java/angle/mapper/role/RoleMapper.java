package angle.mapper.role;

import angle.pojo.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleMapper {
    /*获取角色列表*/
    List<Role> getRoleList();

    /*通过id获取角色*/
    Role getRoleById(@Param("id") int id);


}
