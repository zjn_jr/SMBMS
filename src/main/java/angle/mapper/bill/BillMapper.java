package angle.mapper.bill;

import angle.pojo.Bill;
import angle.pojo.Provider;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface BillMapper {
    /*通过id查询特定的单个订单
     * 但参数仍然是Bill对象，方便以后需要多条件查询时，方便修改
     * 通过动态sql还可以实现billCode和productName查询*/
    Bill queryBillById(Bill bill);

    /*
    获取订单列表,map包含(String productName,int providerId,int isPayment,int currentPageNo,int pageSize)
        1. 查询全部订单
        2. 通过供应商查询
        3. 通过商品名称查询
        4. 通过是否支付查询
        4. 通过供应商+名称+是否支付查询
    */
    List<Bill> getBillList(Map<String,Object> map);

    /*订单添加*/
    int billAdd(Bill bill);

    /*通过id删除订单*/
    int deleteBillById(@Param("id") int id);

    /*通过id修改订单*/
    int modifyBillById(Bill bill);

    /*根据供应商id（providerId）获取订单数量*/
    int getBillCountByProId(@Param("providerId") int providerId);
}
