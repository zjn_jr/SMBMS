package angle.service.bill;

import angle.mapper.bill.BillMapper;
import angle.pojo.Bill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class BillServiceImpl implements BillService{

    @Autowired
    private BillMapper billMapper;

    /*通过id查询特定的单个订单
     * 但参数仍然是Bill对象，方便以后需要多条件查询时，方便修改
     * 通过动态sql还可以实现billCode和productName查询*/
    public Bill queryBillById(Bill bill) {
        return billMapper.queryBillById(bill);
    }

    /*
    获取订单列表,map包含(String billCode, String productName,int currentPageNo,int pageSize)
        1. 查询全部订单
        2. 通过编码查询
        3. 通过商品名称查询
        4. 通过编码+名称查询
    */
    public List<Bill> getBillList(Map<String, Object> map) {
        return billMapper.getBillList(map);
    }

    /*订单添加*/
    public int billAdd(Bill bill) {
        return billMapper.billAdd(bill);
    }

    /*通过id删除订单*/
    public int deleteBillById(int id) {
        return billMapper.deleteBillById(id);
    }

    /*通过id修改订单*/
    public int modifyBillById(Bill bill) {
        return billMapper.modifyBillById(bill);
    }

    /*根据供应商id（providerId）获取订单数量*/
    public int getBillCountByProId(int providerId) {
        return billMapper.getBillCountByProId(providerId);
    }

}
