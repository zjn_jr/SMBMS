package angle.service.provider;

import angle.mapper.provider.ProviderMapper;
import angle.pojo.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ProviderServiceImpl implements ProviderService{

    @Autowired
    private ProviderMapper providerMapper;

    /*通过id查询特定的单个供应商
     * 但参数仍然是provider对象，方便以后需要多条件查询时，方便修改
     * 通过动态sql还可以实现proCode和proName但查询
     */
    public Provider queryProById(Provider provider) {
        return providerMapper.queryProById(provider);
    }

    /*
    获取供应商列表（在mapper的基础上增加分页实现）
    map包含(String proCode, String proName,int currentPageNo,int pageSize)
        1. 查询全部供应商
        2. 通过编码查询
        3. 通过名称查询
        4. 通过编码+名称查询
    */
    public List<Provider> getProList(Map<String, Object> map) {
        return providerMapper.getProList(map);
    }

    /*不包含分页的获取供应商列表,map包含(String proCode, String proName)
        1. 查询全部供应商
        2. 通过编码查询
        3. 通过名称查询
        4. 通过编码+名称查询
    */
    public List<Provider> getProListNoLimit(Map<String,Object> map){
        return providerMapper.getProListNoLimit(map);
    }


    /*供应商添加*/
    public int proAdd(Provider provider) {
        return providerMapper.proAdd(provider);
    }

    /*通过id删除供应商*/
    public int deleteProById(int id) {
        return providerMapper.deleteProById(id);
    }

    /*通过编码proCode或姓名查询用户（没有模糊查询）*/
    public Provider queryProNoLike(Provider provider){
        return providerMapper.queryProNoLike(provider);
    }

    /*根据供应商编码或名称获取用户数量,map包含（String proCode, String proName）*/
    public int queryProCount(Map<String, Object> map) {
        return providerMapper.queryProCount(map);
    }

    /*通过id修改供应商*/
    public int modifyProById(Provider provider) {
        return providerMapper.modifyProById(provider);
    }
}
