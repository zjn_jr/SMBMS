package angle.service.user;

import angle.mapper.user.UserMapper;
import angle.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    /*用户登录验证*/
    public User queryUserById(User user) {
        return userMapper.queryUserById(user);
    }

    /*修改用户密码map包含（int id,String password）*/
    public int updatePwd(Map<String, Object> map) {
        return userMapper.updatePwd(map);
    }

    /*根据用户名或角色获取用户数量map包含（String userName,int userRole）*/
    public int queryUserCount(Map<String, Object> map) {
        return userMapper.queryUserCount(map);
    }

    /*获取全部用户（在mapper的基础上增加分页实现）
    map包含(String userName,int userRole,int currentPageNo,int pageSize)*/
    public List<User> getUserList(Map<String, Object> map){
        return userMapper.getUserList(map);
    }

    /*用户添加*/
    public int userAdd(User user) {
        return userMapper.userAdd(user);
    }

    /*通过id删除用户*/
    public int deleteUserById(int id) {
        return userMapper.deleteUserById(id);
    }

    /*通过id修改用户*/
    public int modifyUserById(User user) {
        return userMapper.modifyUserById(user);
    }




}
