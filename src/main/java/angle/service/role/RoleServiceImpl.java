package angle.service.role;

import angle.mapper.role.RoleMapper;
import angle.pojo.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService{
    @Autowired
    private RoleMapper roleMapper;

    /*获取角色列表*/
    public List<Role> getRoleList() {
        return roleMapper.getRoleList();
    }

    /*通过id获取角色*/
    public Role getRoleById(int id) {
        return roleMapper.getRoleById(id);
    }

}
