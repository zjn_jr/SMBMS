---
 

---

# 超市订单管理系统——SMBMS

### supermarket bill manage system



# 一、 建立包结构









# 二、 导入依赖和配置文件

## 1. pom.xml

maven配置：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>zjn.com</groupId>
    <artifactId>SMBMS</artifactId>
    <version>1.0-SNAPSHOT</version>

    <!--依赖导入：junit，数据库驱动，连接池，servlet，jsp，mybatis，mybatis-spring，spring-->
    <dependencies>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.21</version>
        </dependency>
        <!--数据库连接池-->
        <dependency>
            <groupId>com.mchange</groupId>
            <artifactId>c3p0</artifactId>
            <version>0.9.5.5</version>
        </dependency>

        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
            <version>2.5</version>
        </dependency>
        <dependency>
            <groupId>javax.servlet.jsp</groupId>
            <artifactId>jsp-api</artifactId>
            <version>2.1.3-b03</version>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>jstl</artifactId>
            <version>1.2</version>
        </dependency>

        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis</artifactId>
            <version>3.5.5</version>
        </dependency>
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis-spring</artifactId>
            <version>2.0.5</version>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.12</version>
        </dependency>
        <!--log4j日志-->
        <dependency>
            <groupId>log4j</groupId>
            <artifactId>log4j</artifactId>
            <version>1.2.17</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>5.2.8.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
            <version>5.2.9.RELEASE</version>
        </dependency>
    </dependencies>

    <!--静态资源导出问题-->
    <build>
        <resources>
            <resource>
                <directory>src/main/java</directory>
                <includes>
                    <include>**/*.properties</include>
                    <include>**/*.xml</include>
                </includes>
                <filtering>false</filtering>
            </resource>
            <resource>
                <directory>src/main/resources</directory>
                <includes>
                    <include>**/*.properties</include>
                    <include>**/*.xml</include>
                </includes>
                <filtering>false</filtering>
            </resource>
        </resources>
    </build>

</project>
```

静态资源：

```xml
<!--静态资源导出问题-->
    <build>
        <resources>
            <resource>
                <directory>src/main/java</directory>
                <includes>
                    <include>**/*.properties</include>
                    <include>**/*.xml</include>
                </includes>
                <filtering>false</filtering>
            </resource>
            <resource>
                <directory>src/main/resources</directory>
                <includes>
                    <include>**/*.properties</include>
                    <include>**/*.xml</include>
                </includes>
                <filtering>false</filtering>
            </resource>
        </resources>
    </build>

</project>
```



## 2. mybatis-config.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>

    <!--配置数据源交给spring去做，mybatis的配置文件就只需要出现下面三个配置即可-->

    <settings>
        <!-- <setting name="logImpl" value="STDOUT_LOGGING"/>-->
        <setting name="logImpl" value="LOG4J"/>
    </settings>

    <typeAliases>
        <package name="angle.pojo"/>
    </typeAliases>

    <!--<mappers>

    </mappers>-->

</configuration>
```



## 3. db.properties

```xml
jdbc.driver=com.mysql.cj.jdbc.Driver
jdbc.url=jdbc:mysql://localhost:3306/smbms?useSSl=true&useUnicode=true&characterEncoding=utf8&serverTimezone=UTC
jdbc.username=root
jdbc.password=zjn20000725
```



## 4. spring-dao.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
        https://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context https://www.springframework.org/schema/context/spring-context.xsd">

    <!--1. 关联数据库配置文件-->
    <context:property-placeholder location="classpath:db.properties"/>

    <!--
        2. 连接池
        包括：
        dbcp：半自动化操作，不能自动连接
        c3p0：自动化操作（自动化的加载配置文件，并且可以自动设置到对象中）
        druid，hikari：这两个是底层和springboot中的
        这里使用的是c3p0
    -->
    <bean id="datasource" class="com.mchange.v2.c3p0.ComboPooledDataSource">
        <property name="driverClass" value="${jdbc.driver}"/>
        <property name="jdbcUrl" value="${jdbc.url}"/>
        <property name="user" value="${jdbc.username}"/>
        <property name="password" value="${jdbc.password}"/>

        <!-- c3p0连接池的私有属性 -->
        <property name="maxPoolSize" value="30"/>
        <property name="minPoolSize" value="10"/>
        <!-- 关闭连接后不自动commit -->
        <property name="autoCommitOnClose" value="false"/>
        <!-- 获取连接超时时间 -->
        <property name="checkoutTimeout" value="10000"/>
        <!-- 当获取连接失败重试次数 -->
        <property name="acquireRetryAttempts" value="2"/>
    </bean>

    <!--3. sqlSessionFactory-->
    <bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
        <property name="dataSource" ref="datasource"/>
        <!--绑定mybatis的配置文件-->
        <property name="configLocation" value="classpath:mybatis-config.xml"/>
        <property name="mapperLocations" value="classpath:angle/mapper/*.xml"/>
    </bean>

    <!--4. 配置dao接口扫描包，动态实现了Dao接口可有注入到Spring容器中-->
    <bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
        <!--注入sqlSessionFactory-->
        <property name="sqlSessionFactoryBeanName" value="sqlSessionFactory"/>
        <!--配置要扫描的dao包-->
        <property name="basePackage" value="angle.mapper"/>
    </bean>

</beans>
```



## 5. spring-service.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
        https://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context https://www.springframework.org/schema/context/spring-context.xsd">

    <!--1. 扫描service下的包-->
    <context:component-scan base-package="angle.service"/>

    <!--2. 将所有业务类注入到Spring，可以通过配置或注解实现-->
    <!--<bean id="BookServiceImpl" class="angle.service.BookServiceImpl">
        <property name="bookMapper" ref="bookMapper"/>
    </bean>-->

    <!--3. 声明式事务配置-->
    <bean class="org.springframework.jdbc.datasource.DataSourceTransactionManager" id="dataSourceTransactionManager">
        <!--注入数据源-->
        <property name="dataSource" ref="datasource"/>
    </bean>

    <!--4. aop事务支持-->

</beans>
```



## 6. spring-mvc.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/context
       http://www.springframework.org/schema/context/spring-context.xsd
       http://www.springframework.org/schema/mvc
       https://www.springframework.org/schema/mvc/spring-mvc.xsd">

    <!--1. 注解驱动-->
    <mvc:annotation-driven/>

    <!--2. 静态资源过滤-->
    <mvc:default-servlet-handler/>

    <!--3. 扫描包：controller-->
    <context:component-scan base-package="angle.controller"/>

    <!--4. 视图解析器-->
    <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <property name="prefix" value="/WEB-INF/jsp/"/>
        <property name="suffix" value=".jsp"/>
    </bean>

</beans>
```



## 7. applicationContext.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
        https://www.springframework.org/schema/beans/spring-beans.xsd">

    <import resource="classpath:spring-dao.xml"/>
    <import resource="classpath:spring-service.xml"/>
    <import resource="classpath:spring-mvc.xml"/>

</beans>
```



## 8. web.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">

    <!--1. DispatcherServlet-->
    <servlet>
        <servlet-name>DispatcherServlet</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <init-param>
            <param-name>contextConfigLocation</param-name>
            <!--一定要注意:我们这里加载的是总的配置文件，之前被这里坑了！-->
            <param-value>classpath:applicationContext.xml</param-value>
        </init-param>
        <load-on-startup>1</load-on-startup>
    </servlet>
    <servlet-mapping>
        <servlet-name>DispatcherServlet</servlet-name>
        <url-pattern>/</url-pattern>
    </servlet-mapping>

    <!--2. encodingFilter解决乱码问题-->
    <filter>
        <filter-name>encodingFilter</filter-name>
        <filter-class>
            org.springframework.web.filter.CharacterEncodingFilter
        </filter-class>
        <init-param>
            <param-name>encoding</param-name>
            <param-value>utf-8</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>encodingFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>

    <!--3. 设置Session过期时间-->
    <session-config>
        <session-timeout>15</session-timeout>
    </session-config>

</web-app>
```



## 9. log4j.properties

mybatis-sql日志文件

```xml
#将等级为DEBUG的日志信息输出到console和file这两个目的地，console和file的定义在下面的代码
log4j.rootLogger=DEBUG,console,file

#控制台输出的相关设置
log4j.appender.console = org.apache.log4j.ConsoleAppender
log4j.appender.console.Target = System.out
log4j.appender.console.Threshold=DEBUG
log4j.appender.console.layout = org.apache.log4j.PatternLayout
log4j.appender.console.layout.ConversionPattern=【%c】-%m%n

#文件输出的相关设置
log4j.appender.file = org.apache.log4j.RollingFileAppender
log4j.appender.file.File=./log/zjn.log
log4j.appender.file.MaxFileSize=10mb
log4j.appender.file.Threshold=DEBUG
log4j.appender.file.layout=org.apache.log4j.PatternLayout
log4j.appender.file.layout.ConversionPattern=【%p】【%d{yy-MM-dd}】【%c】%m%n
#让文件每次覆盖重写而不是衔接
log4j.appender.file.append = false

#日志输出级别
log4j.logger.org.mybatis=DEBUG
log4j.logger.java.sql=DEBUG
log4j.logger.java.sql.Statement=DEBUG
log4j.logger.java.sql.ResultSet=DEBUG
log4j.logger.java.sql.PreparedStatement=DEBUG
```



## 10. 在结构中创建lib并导入jar包



# 三、 创建数据库环境

```sql
CREATE DATABASE `smbms`;

USE `smbms`;

DROP TABLE IF EXISTS `smbms_address`;

CREATE TABLE `smbms_address` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `contact` VARCHAR(15) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '联系人姓名',
  `addressDesc` VARCHAR(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收货地址明细',
  `postCode` VARCHAR(15) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '邮编',
  `tel` VARCHAR(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '联系人电话',
  `createdBy` BIGINT(20) DEFAULT NULL COMMENT '创建者',
  `creationDate` DATETIME DEFAULT NULL COMMENT '创建时间',
  `modifyBy` BIGINT(20) DEFAULT NULL COMMENT '修改者',
  `modifyDate` DATETIME DEFAULT NULL COMMENT '修改时间',
  `userId` BIGINT(20) DEFAULT NULL COMMENT '用户ID',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT  INTO `smbms_address`(`id`,`contact`,`addressDesc`,`postCode`,`tel`,`createdBy`,`creationDate`,`modifyBy`,`modifyDate`,`userId`) VALUES (1,'王丽','北京市东城区东交民巷44号','100010','13678789999',1,'2016-04-13 00:00:00',NULL,NULL,1),(2,'张红丽','北京市海淀区丹棱街3号','100000','18567672312',1,'2016-04-13 00:00:00',NULL,NULL,1),(3,'任志强','北京市东城区美术馆后街23号','100021','13387906742',1,'2016-04-13 00:00:00',NULL,NULL,1),(4,'曹颖','北京市朝阳区朝阳门南大街14号','100053','13568902323',1,'2016-04-13 00:00:00',NULL,NULL,2),(5,'李慧','北京市西城区三里河路南三巷3号','100032','18032356666',1,'2016-04-13 00:00:00',NULL,NULL,3),(6,'王国强','北京市顺义区高丽营镇金马工业区18号','100061','13787882222',1,'2016-04-13 00:00:00',NULL,NULL,3);


DROP TABLE IF EXISTS `smbms_bill`;

CREATE TABLE `smbms_bill` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `billCode` VARCHAR(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '账单编码',
  `productName` VARCHAR(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商品名称',
  `productDesc` VARCHAR(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商品描述',
  `productUnit` VARCHAR(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商品单位',
  `productCount` DECIMAL(20,2) DEFAULT NULL COMMENT '商品数量',
  `totalPrice` DECIMAL(20,2) DEFAULT NULL COMMENT '商品总额',
  `isPayment` INT(10) DEFAULT NULL COMMENT '是否支付（1：未支付 2：已支付）',
  `createdBy` BIGINT(20) DEFAULT NULL COMMENT '创建者（userId）',
  `creationDate` DATETIME DEFAULT NULL COMMENT '创建时间',
  `modifyBy` BIGINT(20) DEFAULT NULL COMMENT '更新者（userId）',
  `modifyDate` DATETIME DEFAULT NULL COMMENT '更新时间',
  `providerId` BIGINT(20) DEFAULT NULL COMMENT '供应商ID',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT  INTO `smbms_bill`(`id`,`billCode`,`productName`,`productDesc`,`productUnit`,`productCount`,`totalPrice`,`isPayment`,`createdBy`,`creationDate`,`modifyBy`,`modifyDate`,`providerId`) VALUES (2,'BILL2016_002','香皂、肥皂、药皂','日用品-皂类','块','1000.00','10000.00',2,1,'2016-03-23 04:20:40',NULL,NULL,13),(3,'BILL2016_003','大豆油','食品-食用油','斤','300.00','5890.00',2,1,'2014-12-14 13:02:03',NULL,NULL,6),(4,'BILL2016_004','橄榄油','食品-进口食用油','斤','200.00','9800.00',2,1,'2013-10-10 03:12:13',NULL,NULL,7),(5,'BILL2016_005','洗洁精','日用品-厨房清洁','瓶','500.00','7000.00',2,1,'2014-12-14 13:02:03',NULL,NULL,9),(6,'BILL2016_006','美国大杏仁','食品-坚果','袋','300.00','5000.00',2,1,'2016-04-14 06:08:09',NULL,NULL,4),(7,'BILL2016_007','沐浴液、精油','日用品-沐浴类','瓶','500.00','23000.00',1,1,'2016-07-22 10:10:22',NULL,NULL,14),(8,'BILL2016_008','不锈钢盘碗','日用品-厨房用具','个','600.00','6000.00',2,1,'2016-04-14 05:12:13',NULL,NULL,14),(9,'BILL2016_009','塑料杯','日用品-杯子','个','350.00','1750.00',2,1,'2016-02-04 11:40:20',NULL,NULL,14),(10,'BILL2016_010','豆瓣酱','食品-调料','瓶','200.00','2000.00',2,1,'2013-10-29 05:07:03',NULL,NULL,8),(11,'BILL2016_011','海之蓝','饮料-国酒','瓶','50.00','10000.00',1,1,'2016-04-14 16:16:00',NULL,NULL,1),(12,'BILL2016_012','芝华士','饮料-洋酒','瓶','20.00','6000.00',1,1,'2016-09-09 17:00:00',NULL,NULL,1),(13,'BILL2016_013','长城红葡萄酒','饮料-红酒','瓶','60.00','800.00',2,1,'2016-11-14 15:23:00',NULL,NULL,1),(14,'BILL2016_014','泰国香米','食品-大米','斤','400.00','5000.00',2,1,'2016-10-09 15:20:00',NULL,NULL,3),(15,'BILL2016_015','东北大米','食品-大米','斤','600.00','4000.00',2,1,'2016-11-14 14:00:00',NULL,NULL,3),(16,'BILL2016_016','可口可乐','饮料','瓶','2000.00','6000.00',2,1,'2012-03-27 13:03:01',NULL,NULL,2),(17,'BILL2016_017','脉动','饮料','瓶','1500.00','4500.00',2,1,'2016-05-10 12:00:00',NULL,NULL,2),(18,'BILL2016_018','哇哈哈','饮料','瓶','2000.00','4000.00',2,1,'2015-11-24 15:12:03',NULL,NULL,2);

DROP TABLE IF EXISTS `smbms_provider`;

CREATE TABLE `smbms_provider` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `proCode` VARCHAR(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '供应商编码',
  `proName` VARCHAR(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '供应商名称',
  `proDesc` VARCHAR(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '供应商详细描述',
  `proContact` VARCHAR(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '供应商联系人',
  `proPhone` VARCHAR(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '联系电话',
  `proAddress` VARCHAR(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '地址',
  `proFax` VARCHAR(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '传真',
  `createdBy` BIGINT(20) DEFAULT NULL COMMENT '创建者（userId）',
  `creationDate` DATETIME DEFAULT NULL COMMENT '创建时间',
  `modifyDate` DATETIME DEFAULT NULL COMMENT '更新时间',
  `modifyBy` BIGINT(20) DEFAULT NULL COMMENT '更新者（userId）',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT  INTO `smbms_provider`(`id`,`proCode`,`proName`,`proDesc`,`proContact`,`proPhone`,`proAddress`,`proFax`,`createdBy`,`creationDate`,`modifyDate`,`modifyBy`) VALUES (1,'BJ_GYS001','北京三木堂商贸有限公司','长期合作伙伴，主营产品:茅台、五粮液、郎酒、酒鬼酒、泸州老窖、赖茅酒、法国红酒等','张国强','13566667777','北京市丰台区育芳园北路','010-58858787',1,'2013-03-21 16:52:07',NULL,NULL),(2,'HB_GYS001','石家庄帅益食品贸易有限公司','长期合作伙伴，主营产品:饮料、水饮料、植物蛋白饮料、休闲食品、果汁饮料、功能饮料等','王军','13309094212','河北省石家庄新华区','0311-67738876',1,'2016-04-13 04:20:40',NULL,NULL),(3,'GZ_GYS001','深圳市泰香米业有限公司','初次合作伙伴，主营产品：良记金轮米,龙轮香米等','郑程瀚','13402013312','广东省深圳市福田区深南大道6006华丰大厦','0755-67776212',1,'2014-03-21 16:56:07',NULL,NULL),(4,'GZ_GYS002','深圳市喜来客商贸有限公司','长期合作伙伴，主营产品：坚果炒货.果脯蜜饯.天然花茶.营养豆豆.特色美食.进口食品.海味零食.肉脯肉','林妮','18599897645','广东省深圳市福龙工业区B2栋3楼西','0755-67772341',1,'2013-03-22 16:52:07',NULL,NULL),(5,'JS_GYS001','兴化佳美调味品厂','长期合作伙伴，主营产品：天然香辛料、鸡精、复合调味料','徐国洋','13754444221','江苏省兴化市林湖工业区','0523-21299098',1,'2015-11-22 16:52:07',NULL,NULL),(6,'BJ_GYS002','北京纳福尔食用油有限公司','长期合作伙伴，主营产品：山茶油、大豆油、花生油、橄榄油等','马莺','13422235678','北京市朝阳区珠江帝景1号楼','010-588634233',1,'2012-03-21 17:52:07',NULL,NULL),(7,'BJ_GYS003','北京国粮食用油有限公司','初次合作伙伴，主营产品：花生油、大豆油、小磨油等','王驰','13344441135','北京大兴青云店开发区','010-588134111',1,'2016-04-13 00:00:00',NULL,NULL),(8,'ZJ_GYS001','慈溪市广和绿色食品厂','长期合作伙伴，主营产品：豆瓣酱、黄豆酱、甜面酱，辣椒，大蒜等农产品','薛圣丹','18099953223','浙江省宁波市慈溪周巷小安村','0574-34449090',1,'2013-11-21 06:02:07',NULL,NULL),(9,'GX_GYS001','优百商贸有限公司','长期合作伙伴，主营产品：日化产品','李立国','13323566543','广西南宁市秀厢大道42-1号','0771-98861134',1,'2013-03-21 19:52:07',NULL,NULL),(10,'JS_GYS002','南京火头军信息技术有限公司','长期合作伙伴，主营产品：不锈钢厨具等','陈女士','13098992113','江苏省南京市浦口区浦口大道1号新城总部大厦A座903室','025-86223345',1,'2013-03-25 16:52:07',NULL,NULL),(11,'GZ_GYS003','广州市白云区美星五金制品厂','长期合作伙伴，主营产品：海绵床垫、坐垫、靠垫、海绵枕头、头枕等','梁天','13562276775','广州市白云区钟落潭镇福龙路20号','020-85542231',1,'2016-12-21 06:12:17',NULL,NULL),(12,'BJ_GYS004','北京隆盛日化科技','长期合作伙伴，主营产品：日化环保清洗剂，家居洗涤专卖、洗涤用品网、墙体除霉剂、墙面霉菌清除剂等','孙欣','13689865678','北京市大兴区旧宫','010-35576786',1,'2014-11-21 12:51:11',NULL,NULL),(13,'SD_GYS001','山东豪克华光联合发展有限公司','长期合作伙伴，主营产品：洗衣皂、洗衣粉、洗衣液、洗洁精、消杀类、香皂等','吴洪转','13245468787','山东济阳济北工业区仁和街21号','0531-53362445',1,'2015-01-28 10:52:07',NULL,NULL),(14,'JS_GYS003','无锡喜源坤商行','长期合作伙伴，主营产品：日化品批销','周一清','18567674532','江苏无锡盛岸西路','0510-32274422',1,'2016-04-23 11:11:11',NULL,NULL),(15,'ZJ_GYS002','乐摆日用品厂','长期合作伙伴，主营产品：各种中、高档塑料杯，塑料乐扣水杯（密封杯）、保鲜杯（保鲜盒）、广告杯、礼品杯','王世杰','13212331567','浙江省金华市义乌市义东路','0579-34452321',1,'2016-08-22 10:01:30',NULL,NULL);


DROP TABLE IF EXISTS `smbms_role`;

CREATE TABLE `smbms_role` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `roleCode` VARCHAR(15) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '角色编码',
  `roleName` VARCHAR(15) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '角色名称',
  `createdBy` BIGINT(20) DEFAULT NULL COMMENT '创建者',
  `creationDate` DATETIME DEFAULT NULL COMMENT '创建时间',
  `modifyBy` BIGINT(20) DEFAULT NULL COMMENT '修改者',
  `modifyDate` DATETIME DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT  INTO `smbms_role`(`id`,`roleCode`,`roleName`,`createdBy`,`creationDate`,`modifyBy`,`modifyDate`) VALUES (1,'SMBMS_ADMIN','系统管理员',1,'2016-04-13 00:00:00',NULL,NULL),(2,'SMBMS_MANAGER','经理',1,'2016-04-13 00:00:00',NULL,NULL),(3,'SMBMS_EMPLOYEE','普通员工',1,'2016-04-13 00:00:00',NULL,NULL);


DROP TABLE IF EXISTS `smbms_user`;

CREATE TABLE `smbms_user` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `userCode` VARCHAR(15) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '用户编码',
  `userName` VARCHAR(15) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '用户名称',
  `userPassword` VARCHAR(15) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '用户密码',
  `gender` INT(10) DEFAULT NULL COMMENT '性别（1:女、 2:男）',
  `birthday` DATE DEFAULT NULL COMMENT '出生日期',
  `phone` VARCHAR(15) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '手机',
  `address` VARCHAR(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '地址',
  `userRole` BIGINT(20) DEFAULT NULL COMMENT '用户角色（取自角色表-角色id）',
  `createdBy` BIGINT(20) DEFAULT NULL COMMENT '创建者（userId）',
  `creationDate` DATETIME DEFAULT NULL COMMENT '创建时间',
  `modifyBy` BIGINT(20) DEFAULT NULL COMMENT '更新者（userId）',
  `modifyDate` DATETIME DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT  INTO `smbms_user`(`id`,`userCode`,`userName`,`userPassword`,`gender`,`birthday`,`phone`,`address`,`userRole`,`createdBy`,`creationDate`,`modifyBy`,`modifyDate`) VALUES (1,'admin','系统管理员','1234567',1,'1983-10-10','13688889999','北京市海淀区成府路207号',1,1,'2013-03-21 16:52:07',NULL,NULL),(2,'liming','李明','0000000',2,'1983-12-10','13688884457','北京市东城区前门东大街9号',2,1,'2014-12-31 19:52:09',NULL,NULL),(5,'hanlubiao','韩路彪','0000000',2,'1984-06-05','18567542321','北京市朝阳区北辰中心12号',2,1,'2014-12-31 19:52:09',NULL,NULL),(6,'zhanghua','张华','0000000',1,'1983-06-15','13544561111','北京市海淀区学院路61号',3,1,'2013-02-11 10:51:17',NULL,NULL),(7,'wangyang','王洋','0000000',2,'1982-12-31','13444561124','北京市海淀区西二旗辉煌国际16层',3,1,'2014-06-11 19:09:07',NULL,NULL),(8,'zhaoyan','赵燕','0000000',1,'1986-03-07','18098764545','北京市海淀区回龙观小区10号楼',3,1,'2016-04-21 13:54:07',NULL,NULL),(10,'sunlei','孙磊','0000000',2,'1981-01-04','13387676765','北京市朝阳区管庄新月小区12楼',3,1,'2015-05-06 10:52:07',NULL,NULL),(11,'sunxing','孙兴','0000000',2,'1978-03-12','13367890900','北京市朝阳区建国门南大街10号',3,1,'2016-11-09 16:51:17',NULL,NULL),(12,'zhangchen','张晨','0000000',1,'1986-03-28','18098765434','朝阳区管庄路口北柏林爱乐三期13号楼',3,1,'2016-08-09 05:52:37',1,'2016-04-14 14:15:36'),(13,'dengchao','邓超','0000000',2,'1981-11-04','13689674534','北京市海淀区北航家属院10号楼',3,1,'2016-07-11 08:02:47',NULL,NULL),(14,'yangguo','杨过','0000000',2,'1980-01-01','13388886623','北京市朝阳区北苑家园茉莉园20号楼',3,1,'2015-02-01 03:52:07',NULL,NULL),(15,'zhaomin','赵敏','0000000',1,'1987-12-04','18099897657','北京市昌平区天通苑3区12号楼',2,1,'2015-09-12 12:02:12',NULL,NULL);
```



# 四、 登录实现

将login.jsp设置为首页

得到登录用户：UserMapper->xml->service->impl

登录验证：UserController

- 如果账号和密码都正确就会返回用户，否则返回空


- 返回值是查询到的用户对象user，其userName将会显示在页面frame和head中


注销实现：点击退出按钮，移除保存的session，返回到登录页面



登录拦截：

- ​	保证在未登录或退出登录后，当用户通过网址强制访问页面时，能进行有效的拦截

- ​	编写拦截器，是为登录的用户跳转到error页面

- ​	在springmvc配置文件中配置拦截器文件
- ​	在web.xml中添加不需要被视图解析器过滤的文件:**一定要在视图解析器前设置**
- ​	在error页面设置返回路径：需要在usercontroller中添加一个进入登录页面的请求toLogin



# 五、 密码修改实现

- 在usermapper中添加修改用户密码
- 完善service层
- 在head.jsp中配置设置密码修改页面跳转路径
- 在userController中添加跳转页面请求toModifypwd
- 在pwdmodify.jsp中设置表单提交路径
- jQuery动态实现原密码判断
- 注意：这里使用判断密码是否符合规则使用的是ajax动态判断，在pwdmodify.js中
- 在pwdmodify.jsp页面中有一个隐藏域method，savepwd对应的是密码修改请求，oldpwd对应的是pwdmodify.js实现的动态原密码判断
- 当用户修改成功后，虽然session失效，但还是会先留在修改页面并通过message提示用户修改成功，让用户手动刷新从而重新登录使用（在未重新登录前，就算点击其它功能还是会跳到404页面）



# 六、 用户管理实现

## 1. 功能分析

- 查询：查询全部用户，通过用户名或角色名或联合查询用户 
- 添加用户：跳转添加页面，有一个添加
- 操作栏：修改，删除（需要将当前栏的用户信息也一起传给后端）
- 分页：需要查询用户总数

![截屏2020-10-19 16.14.04](/Users/zjn.jr/Documents/%E7%AC%94%E8%AE%B0/SMBMS%E6%88%AA%E5%9B%BE/%E6%88%AA%E5%B1%8F2020-10-19%2016.14.04.png)



## 2. 界面

- 用户管理：userlist.jsp
  - 第一步需要实现全部用户查询（用户列表），全部角色查询（角色列表），用户总数查询（实现分页）
- 用户添加：useradd.jsp
- 用户查看：userview.jsp
- 用户修改：usermodify.jsp



## 3. 用户管理页面实现

### 1. 用户列表页面实现

1. 获取用户数量：queryUserCount方法获得用户总数（使用了动态sql和模糊查询）

2. 获取用户列表

   - 在tools包中完成分页工具类PageSupport
   - 在sql语句中完成分页需要多穿入pageNo和pageSize（当前页面起始值和页面大小）

   

3. 获取角色列表

   - 因为角色是一个新的实体类，所以需要建新的包
   - 在mapper层和service层分别建role包，保证和pojo一一对应
   - 完成代码

4. 为头部head.jsp添加跳转信息：/user/userlist=>在点击用户管理时就直接进入方法（需要多加一个method=query），查询在返回给userlist页面，这样在点击到同时，userlist.jsp页面到form表单也会处理/user/userlist请求

5. 在UserController中完成/userlist请求

   - 用户总数
   - 用户列表
   - 角色列表
   - 页面当前页数currentPageNo，页面每页显示个数pageSize等前端数据判断后传会前端



## 4. 用户添加

1. 在mapper和service的user中添加useradd方法

2. 添加跳转页面：/touseradd

3. 在controller层完成/useradd

   - 添加方法：当method = add，代表输入全部合法，可以进行添加操作。但是前端传来的都是String，而birthday为date类型，所以不让让其前端的name = birthday（因为如果等于则会直接存入user对象中从而导致错误），而是name = birthdays，在controller中取得后在进行转换并set进user中

   ```html
   <input type="text" Class="Wdate" id="birthday" name="birthdays"
   					readonly="readonly" onclick="WdatePicker();">
   ```

   ```java
   String birthday = request.getParameter("birthdays");
   try {
     user.setBirthday(new SimpleDateFormat("yyyy-MM-dd").parse(birthday));
     System.out.println(birthday);
   } catch (ParseException e) {
     e.printStackTrace();
   }
   ```

   

   - ajax动态获取角色选择栏的参数：method = getrolelist。当进入useradd.jsp时，可以不用刷新页面就能将角色添加进选择栏中
   - ajax动态验证登录账号userCode是否已经存在：method = ucexist。判断当前申请的登录账号是否已被占用。
   - 用户角色显示列需要单独查询，将查出的结果放入user对象的userNameRole属性中，在用model传入前端

解决useradd页面用户角色栏传入json显示中文乱码问题：

```java
response.setContentType("application/json;charset=utf-8");
```

网上的方法说在@RequestMapping中设置，但实测没用

```java
@RequestMapping(value = "/useradd", produces = "application/json; charset=utf-8")
```



## 5. 用户查看

- 在role的mapper和service中实现getRoleById
- UserController中实现/userview
  - 通过code来查询当前查看选中的用户
  - 通过userRole将roleName查出放入user对象的userRoleName中
  - 将查出来user存入model中
  - 将birthday列单独列出来，用SimpleDateFormat转换格式后，在利用model传入前端中



## 6. 用户删除

- 使用ajax在"/userlist"中实现

- 此时method=deluser
- ajax实现在userlist.js中



## 7. 用户修改

- 在user的mapper和service层完成modifyUserById
- controller层使用ajax在"/userlist"中实现
- 此时method = getrolelist
- ajax实现在usermodify.js中
- 注意：
  - id是需要放在隐藏域中的，不然修改会出错
  - birthday需要格式转换



# 八、 供应商管理实现

![截屏2020-10-22 12.42.00](/Users/zjn.jr/Documents/%E7%AC%94%E8%AE%B0/SMBMS%E6%88%AA%E5%9B%BE/%E6%88%AA%E5%B1%8F2020-10-22%2012.42.00.png)

## 1. 功能分析

- 查询出所有的信息并列出到providerlist.jsp
- 查询：通过编码proCode，proName（模糊查询），得到供应商列表
- 点击查看等功能时需要获取对应的供应商：通过id查询，得到单个供应商
- 添加供应商
- 查看：通过id查询
- 修改：通过id修改
- 删除：通过id



## 2. 完成mapper层和service层

代码在provider包中

- ```
  通过id查询特定的单个供应商
  * 但参数仍然是provider对象，方便以后需要多条件查询时，方便修改
  * 通过动态sql还可以实现proCode和proName但查询
  ```

- ```
  获取供应商列表,map包含(String proCode, String proName)
      1. 查询全部供应商
      2. 通过编码查询
      3. 通过名称查询
      4. 通过编码+名称查询
  ```

- ```
  供应商添加（参数为provider对象）
  ```

- ```
  通过id删除供应商
  ```

- ```
  通过id修改供应商
  ```



## 3. controller以及view层（待完善）

#### **ProviderController**

1. 实现供应商管理页面

   - 请求为/pro/prolist
   - 此时方法参数method = query
   - 在head，providerlist中添加跳转方式

2. 实现用户删除

   - 请求依然是/pro/prolist
   - 此时方法参数method = delprovider
   - 使用ajax动态判断：不用刷新页面就能判断删除合法性
   - 实现方法在前端：providerlist.js中
   - 在providerlist.js中：设置url，data
   - **注意：这里需要判断该供应商下是否有订单，如果有则不能删除**
     - 在bill的mapper和service中添加通过providerId获取订单数量的查询
     - 在删除是通过delId判断订单数是否大于0，是则返回订单数
     - 否则成功删除

3. 实现用户添加

   - 需要先在providerlist.jsp设置跳转，使其能进到添加页面（/pro/toproadd）
   - 在controller中完成/toproadd请求
   - 在provideradd.jsp页面设置添加请求（/pro/proadd）
   - 在controller中完成/proadd请求
     - 当所有的字段输入都合法时，执行添加操作：method = add
     - ajax动态验证供应商编码proCode是否已经存在：method = pcexist
     - ajax动态验证供应商名称proName是否已经存在：method = pnexist
       - 这里添加了一个数据库的查询方法，是不带模糊查询的分别通过proCode或proName查找是否有对应的供应商存在

4. 实现订单详情查看

   - 在prolist.js页面设置添加请求（/pro/proview）
   - 在controller中完成/proview请求

5. 实现用户修改

   - 需要现在providerlist.jsp中设置跳转，使其进入到查看页面（/pro/topromodify）
   - 在controller中完成/topromodify请求
   - 在providermodify.jsp页面设置修改请求（/pro/promodify）
   - 在controller中完成/promodify请求





# 九、 订单管理实现

![截屏2020-10-22 22.37.14](/Users/zjn.jr/Documents/%E7%AC%94%E8%AE%B0/SMBMS%E6%88%AA%E5%9B%BE/%E6%88%AA%E5%B1%8F2020-10-22%2022.37.14.png)

## 1. 功能分析

- 查询出所有的信息并列出到billlist.jsp
- 查询：通过productName，billCode（模糊查询），得到供应商列表
- 点击查看等功能时需要获取对应的供应商：通过id,billCode查询，得到单个供应商
- 添加供应商
- 查看：通过id查询
- 修改：通过id修改
- 删除：通过id
- bill数据库中只有供应商id，但是列表需要查出供应商名称providerName=》联合provider，返回一个provider对象（在providerMapper中实现）
- provider的删除需要判断是否有bill存在，需要通过provider的id查询bill的providerId返回一个bill对象



## 2. 完成mapper层和service层

- ```
  通过id查询特定的单个订单
    但参数仍然是Bill对象，方便以后需要多条件查询时，方便修改
    通过动态sql还可以实现billCode和productName查询
  ```

- ```
  获取订单列表,map包含(String billCode, String productName)
      1. 查询全部订单
      2. 通过编码查询
      3. 通过商品名称查询
      4. 通过编码+名称查询
  ```

- ```
  订单添加
  ```

- ```
  通过id删除订单
  ```

- ```
  通过id修改订单
  ```



## 3. controller层以及view层

1. 实现订单管理页面

   - 请求为/bill/billlist
   - 此时方法参数method = query
   - 在head，billlist中添加跳转方式

   

2. 实现订单删除

   - 请求依然是/bill/billist
   - 此时方法参数method = delbill
   - 使用ajax动态判断：不用刷新页面就能判断删除合法性
   - 实现方法在前端：billlist.js中
   - 在billlist.js中：设置url，data

3. 实现订单添加

   - 需要先在billlist.jsp设置跳转，使其能进到添加页面（/bill/tobilladd）
   - 在controller中完成/tobilladd请求
   - 在billadd.jsp页面设置添加请求（/bill/billadd）
   - 在controller中完成/billadd请求
     - 当所有的字段输入都合法时，执行添加操作：method = add
     - ajax动态验证供应商编码proCode是否已经存在：method = pcexist
     - ajax动态验证供应商名称proName是否已经存在：method = pnexist
       - 这里添加了一个数据库的查询方法，是不带模糊查询的分别通过proCode或proName查找是否有对应的供应商存在

4. 实现订单详情查看

   - 在billlist.js页面设置添加请求（/bill/billview）
   - 在controller中完成/billview请求

5. 实现订单修改

   - 需要现在billlist.js中设置跳转，使其进入到查看页面（/bill/tobillmodify）
   - 在controller中完成/tobillmodify请求
   - 在billmodify.jsp页面设置修改请求（/bill/billmodify）
   - 在controller中完成/billmodify请求 
   - 动态获取供应商列表时，可以将url设置为/bill/billadd，因为/billadd中已经有getproviderlist方法了







# 十、 分页完善

## 1. 供应商管理页面分页

- mapper：
  - 和userlist基本一致但是这里在mapper层的getProList加了limit后，会导致billlist页面获取供应商列表失败，所以又加了一个getProListNoLimit来适配bill层
  - 然后还有获取用户数量queryProCount
- service：实现mapper的新增功能
- controller：和user的实现一致



## 2. 订单管理页面分页

- mapper：
  - getBillList中加了limit实现
  - 将原来的getBillCountByproId改为动态sql，当providerId==0时，即变为查询用户总数
- service：实现mapper新增功能
- controller：和user的实现一致





# 注意点

- 用户表里面的birthday虽然没有设置为not null，但是一定不能为空，不然在/user/userlist?method=query（即显示用户列表）时，会因为age为空而出错
- 当点击删除、修改、添加时，如果没有执行成功，也会有弹窗弹出，这时弹窗的“确定”是没有用的，只能点击“取消”



之后会试着实现权限功能








